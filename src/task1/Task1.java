package task1;

import java.util.Scanner;

public class Task1 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String text = scanner.nextLine();
        System.out.println("Кол-во элементов перед точкой = " + text.indexOf("."));
        System.out.println("Кол-во пробелов перед точкой = " + amountOfSpaces(text));

    }

    public static int amountOfSpaces(String text) {
        int amountOfSpaces = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                amountOfSpaces++;
            }
        }
        return amountOfSpaces;

    }
}
