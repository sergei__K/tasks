package task15;

import java.util.Scanner;

public class task15 {
    static Scanner scanner = new Scanner(System.in);

    public static void main (String[]rags){
        System.out.println("Введите кол-во столбцов и строк квадратной матрицы");
        int n = scanner.nextInt();
        int [][] mas1 = new int[n][n];
        int [][] mas2 = new int[n][n];
        filling(mas1);
        out(mas1);
        transposition(mas1, mas2);
        System.out.println();
        out(mas2);
    }

    public static void filling (int[][] mas){
        for (int i=0;i<mas.length;i++){
            for(int j=0;j<mas[i].length;j++){
                mas[i][j] = (int)(Math.random()*10);
            }
        }
    }
    public static void out (int[][] mas) {
        for (int i=0;i<mas.length;i++){
            for(int j=0;j<mas[i].length;j++){
                System.out.print(" " +mas[i][j]);
            }
            System.out.println();
        }
    }


    public static void transposition (int[][] mas1,int[][] mas2){
        for(int i=0;i<mas1.length;i++){
            for(int j=0;j<mas1[i].length;j++){
                mas2[i][j]=mas1[j][i];
            }
        }
    }
}
