package task5;

import java.util.Scanner;

public class Task5 {
    static Scanner scanner = new Scanner(System.in);
    public static void main (String[]args){
        System.out.println("Введите димапазон чисел");
        int n = scanner.nextInt();
        System.out.println("Кол-во простых чисел в диапазоне от 0 до "+ n +" = "+amountOfSimples(n));
    }
    public static boolean checkForSimple (int n) {
        boolean tmp = true;
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                tmp = false;
                break;
            }
        }
        return tmp;
    }
    public static int amountOfSimples(int n){
        int result = 0;
        for(int i=2;i<n;i++){
            if (checkForSimple(i)== true){
                result++;
            }
        }
        return result;
    }
}
