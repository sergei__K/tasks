package task10;

import java.util.Scanner;

public class Task10 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String text = scanner.nextLine();
        System.out.println(isPalindrime(text));
    }

    public static boolean isPalindrime(String text) {
        int i = 0;
        int j = text.length() - 1;
        while (i < j) {
            if (text.charAt(i)!=text.charAt(j)){
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
