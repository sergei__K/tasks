package task9;

import java.util.Scanner;

public class Task9 {
    static Scanner scanner = new Scanner(System.in);
    public static void main (String[]args){
        int [][]mas = new int[5][4];
        filling(mas);
        out(mas);
        System.out.println("Введите номер столбца для обнуления");
        int num = scanner.nextInt()-1;
        vipe(mas,num);
        out(mas);
    }
    public static void filling (int[][]mas){
        for (int i=0;i<mas.length;i++){
            for(int j=0;j<mas[i].length;j++){
                mas[i][j]=(int)(Math.random()*10);
            }
        }
    }
    public static void out (int [][] mas){
        for(int i=0;i<mas.length;i++){
            for(int j=0;j<mas[i].length;j++){
                System.out.print(" "+ mas[i][j]);
            }
            System.out.println();
        }
    }
    public static void vipe (int [][]mas,int num){
        for(int i=0;i<mas.length;i++){
            mas[i][num]=0;
        }
    }
}